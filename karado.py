# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from grab import Grab
import json
import os
import re
import sys
import time

# example url: "http://www.karado.ru/arenda/kvartiry/belgorod"

class Karado:
	main_url = "http://www.karado.ru/"
	system_name = "karado"
	
	supported_cities = ["belgorod"]
	supported_actions = ["buy", "rent"]

	default_ads_per_page = 20
	ads_per_page = default_ads_per_page

	def collect(self, action, city):
		pass

	def grab_main_page(self, action, city):
        	g = Grab()
        	resp = g.go(self.construct_url(action, city))
		main_file_name = "{0}/{0}_{1}_{2}_{3}.html".format(self.system_name,
			action, city,
			int(time.time()))
		g.doc.save(main_file_name)

		return main_file_name

	def page_urls(self, grab, action, city):
        	ads_count = int(grab.doc.rex_search(u"из (\d+)").group(1))
        	page_count = ads_count / self.ads_per_page
        	page_urls = []
        	page_template = self.construct_url(action, city) + "?page={}"

        	for number in xrange(1, page_count + 1):
                	page_urls.append(page_template.format(number))
        	return page_urls

	def grab_pages(self, main_page_file_name, action, city):
        	page_file_names = []
		with open(main_page_file_name) as f:
                	g = Grab(f.read())
                	pages = self.page_urls(g, action, city)
		for page_index in xrange(len(pages)):
                        g = Grab()
                        g.go(pages[page_index])
			#do it in the temporary dir
			temp_dir = "./{0}/".format(self.system_name)
			page_file_name = "{0}_{1}.html".format(action, page_index + 1)
			page_file_names.append(page_file_name)
                        g.doc.save(temp_dir + page_file_name)
			for item_file_name in self.items_from_page(action, page_index):
				print(self.build_item(item_file_name))
		return page_file_names

	def items_from_page(self, action, page_number):
        	item_urls = self.urls_from_pagefile(self.construct_item_filename(action, page_number + 1))
		result = []
        	g = Grab()
        	for item_url in item_urls:
                	resp = g.go("http://www.karado.ru" + item_url)
			_,item_id = re.match("(.*)/(\d+)", item_url).groups()
			item_file_name = "{0}/{1}_{2}_{3}.html".format(self.system_name, action, page_number + 1, item_id)
                	g.doc.save(item_file_name)
			result.append(item_file_name)
		return result

	def build_item(self, item_file_name):
        	with open(item_file_name) as f:
                	g = Grab(f.read())
                	strongs = g.doc.select("//ul[@class='menu style4']/li/strong")
                	ems = g.doc.select("//ul[@class='menu style4']/li/em")
                	item = {}
                	for i in xrange(len(ems)):
                        	key = strongs[i].text()
                        	value = ems[i].text()

                        	if key == u"Районы Белгорода":
                                	item['district'] = value
                       		elif key == u"Общая площадь, кв.м":
                                	item['area'] = value
                        	elif key == u"Жилая площадь, кв.м":
                                	item['living_area'] = value
                        	elif key == u"Площадь кухни, кв.м":
                                	item['kitchen_area'] = value
                        	elif key == u"Санузел":
                                	item['sanitary'] = value
                        	elif key == u"Балкон/лоджия":
                                	item['balcony'] = value
                        	elif key == u"Этаж":
                                	item['floor'] = value
                        	elif key == u"Этажность":
                                	item['floor_number'] = value
        	return item
	
	def urls_from_pagefile(self, pagefilename):
		result = []
		with open(pagefilename) as f:
			g = Grab(f.read())
			for anchor in g.doc.select('//*[@id="listing"]/tbody/tr/td/h2/a'):
				result.append(anchor.attr('href'))
			return result

	def construct_item_filename(self, action, page):
		return "./{0}/{1}_{2}.html".format(self.system_name, action, page)
	
	def construct_url(self, action, city):
		if action not in self.supported_actions:
			raise ValueError("action {0} not supported".format(action))

		if city not in self.supported_cities:
			raise ValueError("city {0} not supported".format(city))

		return "{0}{1}/kvartiry/{2}".format(self.main_url, 
			"prodazha" if action == "buy" else "arenda", city)
