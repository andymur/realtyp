#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from grab import Grab
import json
import os
import re
import sys
from karado import Karado
import argparse

default_ads_per_page = 20


def grab_items_from_page(page_number):
	try:
		os.mkdir("karado_{}".format(page_number))
	except OSError:
		pass
	
	item_urls = item_urls_from_page(page_number)
	
	g = Grab()
	for item_url in item_urls:
		resp = g.go("http://www.karado.ru" + item_url)
		g.doc.save("karado_{}/{}.html".format(page_number, item_url))

def build_item(file_name):
	with open(file_name) as f:
		g = Grab(f.read())
		strongs = g.doc.select("//ul[@class='menu style4']/li/strong")
		ems = g.doc.select("//ul[@class='menu style4']/li/em")
		item = {}
		for i in xrange(len(ems)):
			key = strongs[i].text()
			value = ems[i].text()
	
			if key == u"Районы Белгорода":
				item['district'] = value
			elif key == u"Общая площадь, кв.м":
				item['area'] = value
			elif key == u"Жилая площадь, кв.м":
				item['living_area'] = value
			elif key == u"Площадь кухни, кв.м":
				item['kitchen_area'] = value
			elif key == u"Санузел":
				item['sanitary'] = value
			elif key == u"Балкон/лоджия":
				item['balcony'] = value
			elif key == u"Этаж":
				item['floor'] = value
			elif key == u"Этажность":
				item['floor_number'] = value
	return item

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='all that magic starts here')

        parser.add_argument('-s', '--source', action = 'store', dest = 's', help = 'source (usually website)')
        parser.add_argument('-c', '--city', dest = 'c', help = 'city from where are data')
	parser.add_argument('-a', '--action', dest = 'a', help = 'action (for now buy or rent)')

        args = parser.parse_args()

	karado = Karado()
	main_page = karado.grab_main_page(args.a, args.c)
	#karado.grab_pages("./karado/karado_rent_belgorod_1454275577.html", args.a, args.c)
	karado.grab_pages(main_page, args.a, args.c)
